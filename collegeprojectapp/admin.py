from django.contrib import admin

# Register your models here.
from .models import FacultyName
class FaculityAdmin(admin.ModelAdmin):
    list_display = ['id','firstname','lastname','username','password1','password2','mobile','email','gender','quali','salary','location']

admin.site.register(FacultyName,FaculityAdmin)