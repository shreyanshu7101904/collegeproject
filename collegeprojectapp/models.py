from django.db import models
from multiselectfield import MultiSelectField

class Studentregistration(models.Model):
    firstname = models.CharField(max_length=30)
    lastname = models.CharField(max_length=30)
    lastname = models.CharField(unique=True,max_length=30)
    password1 = models.CharField(max_length=30,null=True)
    password2 = models.CharField(max_length=30,null=True)
    mobile = models.BigIntegerField(unique=True)
    email = models.EmailField(unique=True)
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES,null=True)


class FacultyName(models.Model):
    firstname = models.CharField(max_length=30)
    lastname = models.CharField(max_length=30)
    username = models.CharField(unique=True, max_length=30)
    password1 = models.CharField(max_length=30,null=True)
    password2=models.CharField(max_length=30,null=True)
    mobile = models.BigIntegerField(unique=True)
    email = models.EmailField(unique=True)
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, null=True)
    quali=models.CharField(max_length=10)
    salary = models.FloatField()

    location=models.CharField(max_length=10)
class FacultyUpdateModel(models.Model):
    username=models.CharField(max_length=30)
    password1=models.CharField(max_length=50)
    password2=models.CharField(max_length=50)

class StudentFeedBackModel(models.Model):
    name=models.CharField(max_length=100)
    date=models.DateField()
    message=models.TextField(max_length=500)
    rating=models.IntegerField()


class AdminLoginModel(models.Model):
    username = models.CharField(unique=True, max_length=30)
    password = models.CharField(max_length=30, null=True)


class registerdata(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    GENDER_CHOICE =(
        ('Male','Male'),
        ('Female', 'Female')
    )
    gender = models.CharField(max_length=10, choices=GENDER_CHOICE)
    username = models.CharField(max_length=20)
    password1 = models.CharField(max_length=20)
    password2 = models.CharField(max_length=20)
    email = models.EmailField(max_length=40)
    mobile = models.BigIntegerField()
