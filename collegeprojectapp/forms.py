from django import forms
from .models import Studentregistration
from multiselectfield import MultiSelectFormField

class StudentRegistrationForm(forms.ModelForm):
    class Meta:
        model = Studentregistration
        fields = '__all__'
        widgets = {
            'firstname': forms.TextInput(attrs={'class': 'form-control ', 'placeholder': 'First Name'}),
            'lastname': forms.TextInput(attrs={'class': 'form-control ', 'placeholder': 'Last Name'}),
            'password1': forms.TextInput(attrs={'class': 'form-control ', 'placeholder': 'Mobile Number'}),
            'password2': forms.TextInput(attrs={'class': 'form-control ', 'placeholder': 'Mobile Number'}),
            'mobile': forms.TextInput(attrs={'class': 'form-control ', 'placeholder': 'Mobile Number'}),
            'gender': forms.SelectField(attrs={'class': 'form-control ', 'placeholder': 'Select your gender'}),
            'email': forms.EmailInput(attrs={'class': 'form-control ', 'placeholder': 'Email Id'}),
        }
    # model = 
    firstname = forms.CharField(
        label = 'Enter Your FirstName',
        widget = forms.TextInput(
            attrs = {
                'class':'form-control',
                'placeholder' : 'FirstName'
            }
        )
    )

    lastname = forms.CharField(
        label='Enter Your LastName',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'LastName'
            }
        )
    )

    username = forms.CharField(
        label='Enter Your Username',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Username'
            }
        )
    )

    password1 = forms.CharField(
        label='Enter Your first  Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Password'
            }
        )
    )
    password2 = forms.CharField(
        label='Enter Your confirm  Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'confirm Password'
            }
        )
    )

    mobile = forms.IntegerField(
        label='Enter Your Mobile',
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Mobile No'
            }
        )
    )


    email = forms.EmailField(
        label='Enter Your Email',
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Email'
            }
        )
    )
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = forms.ChoiceField(
        widget=forms.RadioSelect(), choices=GENDER_CHOICES
    )



class StudentLoginForm(forms.Form):
    username = forms.CharField(
        label='Enter Your Username',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Username'
            }
        )
    )

    password1 = forms.CharField(
        label='Enter Your  Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Password'
            }
        )
    )

class StudentUpdatePasswordForm(forms.Form):
    username = forms.CharField(
        label='Enter Your Username',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Username'
            }
        )
    )

    password1 = forms.CharField(
        label='Enter Your  Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Password'
            }
        )
    )


class FaculityFormLogin(forms.Form):
    username = forms.CharField(
        label='Enter Your Faculty Username',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Faculty Username'
            }
        )
    )
    password1 = forms.CharField(
        label='Enter Your Faculty Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': ' Faculty Password'
            }
        )
    )

class FacaulityupdateForm(forms.Form):
    username = forms.CharField(
        label='Enter Your Faculty Username',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Faculty Username'
            }
        )
    )
    password1 = forms.CharField(
        label='Enter Your Faculty first Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Faculty Username'
            }
        )
    )

class FaculityDetailsForm(forms.Form):
    firstname = forms.CharField(
        label='Enter Your First Name',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Name'
            }
        )
    )
    lastname = forms.CharField(
        label='Enter Your Last Name',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Last Name'
            }
        )
    )
    username = forms.CharField(
        label='Enter Your Faculty Username',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Faculty Username'
            }
        )
    )
    password1 = forms.CharField(
        label='Enter Your Faculty Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': ' Faculty Password'
            }
        )
    )
    password2 = forms.CharField(
        label='Enter Your Faculty confirm Password2',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': ' Faculty confirm Password'
            }
        )
    )
    mobile = forms.IntegerField(
        label='Enter Your mobile number',
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control',
                'placeholder': ' mobile number'
            }
        )
    )
    email = forms.EmailField(
        label='Enter Your Email id',
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Email id'
            }
        )
    )
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = forms.ChoiceField(
        widget=forms.RadioSelect(), choices=GENDER_CHOICES
    )
    quali=forms.CharField(
        label='Enter Your Qualification',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Your Qualification'
            }
        )
    )
    salary = forms.FloatField(
        label='Enter Your Salary',
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Your Salary'
            }
        )
    )
    location = forms.CharField(
        label='Enter Your Location',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Your Qualification'
            }
        )
    )
class StudentFeedbackForm(forms.Form):

    name= forms.CharField(
        label='Enter Your Name',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Your Name'
            }
        )
    )
    message= forms.CharField(
        label='Enter Your Message',
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'placeholder': 'Your Message'
            }
        )
    )
    rating= forms.IntegerField(
        label='Enter Your Rating Number From 1 to 5',
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Your Rating Number From 1 to 5'
            }
        )
    )





#student update data form here
class StudentUpdatingDataForm(forms.Form):
    firstname = forms.CharField(
        label='Enter Your FirstName',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'FirstName'
            }
        )
    )

    email = forms.EmailField(
        label='Enter Your Email',
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'email'
            }
        )
    )


#Admin form here
class AdminLoginForm(forms.Form):
    username = forms.CharField(
        label='Enter Your Admin Username',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Username'
            }
        )
    )
    password = forms.CharField(
        label='Enter Your Admin Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': ' Admin Password'
            }
        )
    )


#AdminAuthority
class RegisterForm(forms.Form):
    firstname = forms.CharField(
        label='Enter Your FirstName',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'FirstName'
            }
        )
    )

    lastname = forms.CharField(
        label='Enter Your LastName',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'LastName'
            }
        )
    )

    username = forms.CharField(
        label='Enter Your Username',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Username'
            }
        )
    )

    password1 = forms.CharField(
        label='Enter Your first  Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Password'
            }
        )
    )
    password2 = forms.CharField(
        label='Enter Your confirm  Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'confirm Password'
            }
        )
    )

    mobile = forms.IntegerField(
        label='Enter Your Mobile',
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Mobile No'
            }
        )
    )

    email = forms.EmailField(
        label='Enter Your Email',
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Email'
            }
        )
    )
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = forms.ChoiceField(
        widget=forms.RadioSelect(), choices=GENDER_CHOICES
    )


