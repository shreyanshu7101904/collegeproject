from django.apps import AppConfig


class CollegeprojectappConfig(AppConfig):
    name = 'collegeprojectapp'
