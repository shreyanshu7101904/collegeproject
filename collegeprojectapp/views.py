from django.shortcuts import render,redirect
from django.views.generic import TemplateView, ListView
from django.http import HttpResponseRedirect
from django.urls import reverse

from .models import Studentregistration,FacultyName,FacultyUpdateModel,StudentFeedBackModel,AdminLoginModel,registerdata
from .forms import StudentRegistrationForm,StudentLoginForm,StudentUpdatingDataForm,FaculityFormLogin,StudentUpdatePasswordForm,FacaulityupdateForm,FaculityDetailsForm,StudentFeedbackForm,AdminLoginForm,RegisterForm
from django.http.response import HttpResponse

import datetime as dt
date1=dt.datetime.now()

class Home(TemplateView):
    template_name = 'Home.html'
    
# def Home(request):
#     return render(request,'Home.html')


class Student(TemplateView):
    template_name = 'StudentLogin.html'

# def Student(request):
#     return render(request,'StudentLogin.html')


class Faculty(TemplateView):
    template_name = "Faculty.html"

# def Faculty(request):
#         return render(request, 'Faculty.html')
#
# def Results(request):
#     return render(request,'Results.html')

class StudentRegView(TemplateView):
    template_name = "StudentRegistration.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["regForm"] = StudentRegistrationForm(self.request.POST or None)
        return context

    def post(self, request):
        context = self.get_context_data()
        if context["regForm"].is_valid():
            context['regForm'].save()
            context['regForm'] = StudentRegistrationForm()
            # messages.add_message(request, messages.SUCCESS, 'Form has been submitted Successfully. We Will get back to you soon.!!')
        return HttpResponseRedirect(reverse('student_reg'))
    

# def StudentRegview(request):
#     if request.method == 'POST':
#         form = StudentRegistrationForm(request.POST)
#         if form.is_valid():
#             form.save()

#             firstname=form.cleaned_data.get('firstname','')
#             lastname=form.cleaned_data.get('lastname','')
#             username=form.cleaned_data.get('username','')
#             password1=form.cleaned_data.get('password1','')
#             password2=form.cleaned_data.get('password2','')
#             mobile=form.cleaned_data.get('mobile','')
#             email=form.cleaned_data.get('email','')
#             gender=form.cleaned_data.get('gender','')
#             data = Studentregistration(
#                 firstname = firstname,
#                 lastname = lastname,
#                 username = username,
#                 password1=password1,
#                 password2=password2,
#                 mobile=mobile,
#                 email=email,
#                 gender=gender
#             )
#             data.save()

#             return redirect('/StudentLogin')
#         else:
#             form = StudentRegistrationForm()
#             return render(request, 'StudentRegistration.html', {'form': form})
#     else:
#         form = StudentRegistrationForm()
#         return render(request, 'StudentRegistration.html', {'form': form})


def StudentLoginview(request):
    if request.method == 'POST':
        slform = StudentLoginForm(request.POST)
        if slform.is_valid():
            username = request.POST.get('username')
            password1 = request.POST.get('password1')
            user1 = Studentregistration.objects.filter(username=username)
            pwd1 = Studentregistration.objects.filter(password1=password1)
            if not user1 and pwd1 :
                return HttpResponse('Invalid Details')

            else:
                return redirect('/NextPage/')
        else:
            slform = StudentLoginForm()
            return render(request,'StudentLogin.html',{'slform':slform})
    else:
        ssform=Studentregistration.objects.all()
        slform = StudentLoginForm()
        return render(request, 'StudentLogin.html', {'slform': slform,'ssform':ssform})


def FacultyLoginVIew(request):
    if request.method=='POST':
        fform=FaculityFormLogin(request.POST)
        if fform.is_valid():
            username=request.POST.get('username')
            password1=request.POST.get('password1')
            user=FacultyName.objects.filter(username=username)
            pwd1=FacultyName.objects.filter(password1=password1)
            if not user and pwd1 :
                return HttpResponse("invalid faculty login please try again")
            else:
                fform=FacultyName.objects.all()
                return  render(request,'faculitydata.html',{'fform':fform})

    else:
        fform=FaculityFormLogin()
        return render(request,'faclutylogin.html',{'fform':fform})


def facalityupdateview(request):
    if request.method=='POST':
        uform=FacaulityupdateForm(request.POST)
        if uform.is_valid():
            username=request.POST.get('username')
            password1=request.POST.get('password1')
            password2=request.POST.get('password2')
            puser=FacultyName.objects.filter(username=username)
            if not puser:
                return HttpResponse('your password is not updated beacause you are not user')
            else:
                puser.update(password1=password1,password2=password2)
                return HttpResponse('Password Has Been Successfully Updated  <a href="/">back</a>')
    else:
        uform=FacaulityupdateForm()
        return render(request,'faculityupdate.html',{'uform':uform})


def updaitfacaulityview(request):
    if request.method=='POST':
        dform=FaculityDetailsForm(request.POST)
        if dform.is_valid():
            firstname=request.POST.get('firstname')
            lastname=request.POST.get('lastname')
            username=request.POST.get('username')
            password1=request.POST.get('password1')
            password2=request.POST.get('password2')
            email=request.POST.get('email')
            mobile=request.POST.get('mobile')
            gender=request.POST.get('gender')
            quali=request.POST.get('quali')
            salary = request.POST.get('salary')
            location=request.POST.get('location')
            puser=FacultyName.objects.filter(username=username)
            if not puser:
                return HttpResponse('this is invalid user that is not update your detais please try again')
            else:
                puser.update(firstname=firstname,lastname=lastname,password1=password1,password2=password2,email=email,mobile=mobile,gender=gender,quali=quali,salary=salary,location=location)
                return HttpResponse('data updates successfully'
                                    '/<a href="/">back</a>')

    else:
        dform=FaculityDetailsForm()
        return render(request,'faculitydeatil.html',{'dform':dform})

def StudentFeedbachView(request):
    if request.method=='POST':
        form=StudentFeedbackForm(request.POST)
        if form.is_valid():
            name=request.POST.get('name')
            message=request.POST.get('message')
            rating=request.POST.get('rating')
            s=StudentFeedBackModel(name=name,date=date1,message=message,rating=rating)
            s.save()
            return redirect('/studentfeedback/')
        else:
            return HttpResponse('your data is not inserted')

    else:
        form=StudentFeedBackModel.objects.all()
        sform=StudentFeedbackForm()
        return render(request,'studentfeedback.html',{'sform':sform,'form':form})




def studentupdatepassword(request):
    if request.method=='POST':
        sform=StudentUpdatePasswordForm(request.POST)
        if sform.is_valid():
            username=request.POST.get('username')
            password1=request.POST.get('password1')

            puser=Studentregistration.objects.filter(username=username)
            if not puser:
                return HttpResponse('your password is not updated beacuse you are not user')
            else:
                puser.update(username=username,password1=password1)
                return HttpResponse('Password Has Been Successfully Updated  <a href="/">back</a>')
    else:
        sform=StudentUpdatePasswordForm()
        return render(request,'Studentforgotpassword.html',{'sform':sform})



#for view student data

def retrieveview(request):
    print(request.user.id)
    ssform = Studentregistration.objects.filter(pk=request.user.id)
    return render(request, 'viewupdate.html', {'ssform': ssform})

def updatingview(request):
    if request.method == "POST":
        uform = StudentUpdatingDataForm(request.POST)
        if uform.is_valid():
            firstname = request.POST.get('firstname')
            email = request.POST.get('email')
            pdata = Studentregistration.objects.filter(firstname=firstname)

            if not pdata:
                return HttpResponse('invaliddata')
            else:
                pdata.update(email=email)
                uform = StudentUpdatingDataForm()
                return render(request, 'updatingdata.html', {'uform': uform})
    else:
        uform = StudentUpdatingDataForm()
        return render(request, 'updatingdata.html', {'uform': uform})


def NextPage(request):
    return render(request, 'NextPage.html')


#Admin login here

def AdminLoginview(request):
    if request.method=="POST":
        lform=AdminLoginForm(request.POST)
        if lform.is_valid():
            uname=request.POST.get("username",'')
            pwd=request.POST.get("password",'')
            uname1=AdminLoginModel.objects.filter(username=uname)
            pwd1=AdminLoginModel.objects.filter(password=pwd)
            if uname1 and pwd1:
                return redirect('/AddFaculty/')
            else:
                return HttpResponse("wrong username and password")
        else:
            return HttpResponse("plz check the username and password")
    else:
        lform=AdminLoginForm()
        return render(request,'AdminLoginForm.html',{'lform':lform})


def view(request):
    return render(request,'AddFaculty.html')


#admin authority

def AddFaculty(request):
    return render(request,'AddFaculty.html')


def regview(request):
    if request.method == 'POST':
        rform = RegisterForm(request.POST)
        if rform.is_valid():
            first_name = request.POST.get('first_name','')
            last_name = request.POST.get('last_name', '')
            gender = rform.cleaned_data.get('gender', '')
            username = request.POST.get('username', '')
            password1 = request.POST.get('password1', '')
            password2 = request.POST.get('password2', '')
            email = request.POST.get('email', '')
            mobile = request.POST.get('mobile', '')
            data = registerdata(
                first_name=first_name,
                last_name=last_name,
                gender=gender,
                username=username,
                password1=password1,
                password2=password2,
                email=email,
                mobile=mobile
            )
            data.save()
            rform = RegisterForm()
            return render(request, 'NewFaculty.html', {'rform':rform})
    else:
        rform = RegisterForm()
        return render(request,'NewFaculty.html',{'rform':rform} )








