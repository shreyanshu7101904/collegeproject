# Generated by Django 2.2.4 on 2019-08-29 04:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('collegeprojectapp', '0007_auto_20190828_2210'),
    ]

    operations = [
        migrations.CreateModel(
            name='Studentforfotpassword',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=30)),
                ('password1', models.CharField(max_length=50)),
                ('password2', models.CharField(max_length=50)),
            ],
        ),
        migrations.RemoveField(
            model_name='studentregistration',
            name='password',
        ),
        migrations.AddField(
            model_name='studentregistration',
            name='password1',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='studentregistration',
            name='password2',
            field=models.CharField(max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='studentregistration',
            name='sroll',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='studentregistration',
            name='gender',
            field=models.CharField(choices=[('M', 'Male'), ('F', 'Female')], max_length=1),
        ),
    ]
