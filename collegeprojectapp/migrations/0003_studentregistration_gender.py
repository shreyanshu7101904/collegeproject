# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2019-07-18 18:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('collegeprojectapp', '0002_remove_studentregistration_gender'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentregistration',
            name='gender',
            field=models.CharField(choices=[('M', 'Male'), ('F', 'Female')], max_length=1, null=True),
        ),
    ]
