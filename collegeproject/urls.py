"""collegeproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from collegeprojectapp import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',views.Home.as_view()),
    url(r'^Student/',views.Student.as_view()),
    url(r'^StudentLogin/', views.StudentLoginview),
    url(r'^Registration/', views.StudentRegView.as_view(), name='student_reg'),
    url(r'^retrieve/',views.retrieveview),
    url(r'^update/', views.updatingview),
    url(r'^faculty/', views.Faculty.as_view()),
    url(r'^FacultyLogin/', views.FacultyLoginVIew),
    url(r'Password/$',views.facalityupdateview),
    url(r'spwd/$',views.studentupdatepassword),
    url(r'^AdminLogin/', views.AdminLoginview),
    url(r'AddFaculty/',views.view),

    url(r'updatdetails/',views.updaitfacaulityview),
    url(r'studentfeedback/',views.StudentFeedbachView),

    url(r'NewFaculty/',views.regview),
    url(r'AddFaculty/',views.AddFaculty),
    url(r'NextPage/',views.NextPage),

]
